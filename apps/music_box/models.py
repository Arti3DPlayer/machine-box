# -*- coding: utf-8 -*-
from django.db import models


class Genre(models.Model):
    name = models.CharField(u'Название жанра', max_length=255)
    name_en = models.CharField(u'Название жанра(en)',
                               max_length=255,
                               help_text=u'Можно использовать только данные жанры: https://vk.com/dev/audio_genres')
    preview = models.ImageField(u'Фото', upload_to='music_box', null=True, blank=True)
    position = models.PositiveIntegerField(u'Позиция на странице', default=0)
    is_hidden = models.BooleanField(u'Скрыть', default=False)

    class Meta:
        verbose_name = u'Жанр'
        verbose_name_plural = u'Жанры музыки'
        permissions = (
            ('view_genre', 'Can view genres'),
        )

    def __unicode__(self):
        return u'Жанр %s' % self.name