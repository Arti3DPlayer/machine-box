# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Genre',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0436\u0430\u043d\u0440\u0430')),
                ('name_en', models.CharField(help_text='\u041c\u043e\u0436\u043d\u043e \u0438\u0441\u043f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u044c \u0442\u043e\u043b\u044c\u043a\u043e \u0434\u0430\u043d\u043d\u044b\u0435 \u0436\u0430\u043d\u0440\u044b: https://vk.com/dev/audio_genres', max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0436\u0430\u043d\u0440\u0430(en)')),
                ('preview', models.ImageField(upload_to=b'music_box', null=True, verbose_name='\u0424\u043e\u0442\u043e', blank=True)),
                ('position', models.PositiveIntegerField(default=0, verbose_name='\u041f\u043e\u0437\u0438\u0446\u0438\u044f \u043d\u0430 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0435')),
                ('is_hidden', models.BooleanField(default=False, verbose_name='\u0421\u043a\u0440\u044b\u0442\u044c')),
            ],
            options={
                'verbose_name': '\u0416\u0430\u043d\u0440',
                'verbose_name_plural': '\u0416\u0430\u043d\u0440\u044b \u043c\u0443\u0437\u044b\u043a\u0438',
            },
            bases=(models.Model,),
        ),
    ]
