# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('music_box', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='genre',
            options={'verbose_name': '\u0416\u0430\u043d\u0440', 'verbose_name_plural': '\u0416\u0430\u043d\u0440\u044b \u043c\u0443\u0437\u044b\u043a\u0438', 'permissions': (('view_genre', 'Can view genres'),)},
        ),
    ]
