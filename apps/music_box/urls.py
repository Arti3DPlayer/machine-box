# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from apps.music_box.views import GenresListView, GenreDetailView,\
    TopTracksListView, RandomTrackView, BuyTrackView, ArtistListView, ArtistDetailView, \
    AlbumListView, AlbumDetailView, SearchTrackView, GetMusicBoxMachineSettingsView,\
    MusicBoxMachineAdvertismentSliderView, SendMoneyView, ConvertCreditToBalanceView, GiveChangeView, \
    GiveChangeConfirmView


urlpatterns = patterns('',
    url(r'genre/list/$', GenresListView.as_view(), name='genre_list'),
    url(r'genre/(?P<pk>\d+)/$', GenreDetailView.as_view(), name='genre_detail'),
    url(r'artist/list/$', ArtistListView.as_view(), name='artist_list'),
    url(r'artist/detail/$', ArtistDetailView.as_view(), name='artist_detail'),
    url(r'album/list/$', AlbumListView.as_view(), name='album_list'),
    url(r'album/detail/$', AlbumDetailView.as_view(), name='album_detail'),
    url(r'top/list/$', TopTracksListView.as_view(), name='top_tracks_list'),
    url(r'search/$', SearchTrackView.as_view(), name='search'),
    url(r'random/track/$', RandomTrackView.as_view(), name='random_track'),
    url(r'buy/track/$', BuyTrackView.as_view(), name='buy_track'),
    url(r'get/settings/$', GetMusicBoxMachineSettingsView.as_view(), name='get_music_box_settings'),
    url(r'advertisment/slider/$', MusicBoxMachineAdvertismentSliderView.as_view(), name='advertisment_slider'),
    url(r'send/money/$', SendMoneyView.as_view(), name='send_money'),
    url(r'convert/credit_to_balance/$', ConvertCreditToBalanceView.as_view(), name='convert_credit_to_balance'),
    url(r'give/change/$', GiveChangeView.as_view(), name='give_change'),
    url(r'give/change/confirm/$', GiveChangeConfirmView.as_view(), name='give_change_confirm'),
)
