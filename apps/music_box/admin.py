# -*- encoding: utf-8 -*-
from django.contrib import admin

from apps.music_box.models import Genre
from guardian.shortcuts import get_objects_for_user
from guardian.admin import GuardedModelAdmin


class GenreAdmin(GuardedModelAdmin):
    model = Genre
    list_display = ('name', 'position', 'is_hidden')

    def get_queryset(self, request):
        return get_objects_for_user(request.user, 'music_box.view_genre')

admin.site.register(Genre, GenreAdmin)
