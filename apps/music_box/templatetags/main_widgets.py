# -*- coding: utf-8 -*-

from django import template
from apps.music_box.forms import SearchForm

register = template.Library()


@register.simple_tag(takes_context=True)
def get_params(context, new=False, **kwargs):
    """
    Для добавления гет параметров до уже существующих
    """
    data = dict()
    if not new:
        data = {k: v for k, v in context['request'].GET.iteritems() if v}
    for key, value in kwargs.iteritems():
        data[key] = value

    if not data:
        return

    params = '?'
    for key, value in data.iteritems():
        params = '%s%s=%s&' % (params, key, value)
    return params[:-1]

@register.inclusion_tag('music_box/inclusion/search_form.html')
def search_form_tag():
    return {
        'form': SearchForm()
    }