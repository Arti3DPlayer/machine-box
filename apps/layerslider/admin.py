# -*- encoding: utf-8 -*-
from django.contrib import admin
from django import forms
from django.utils.translation import ugettext_lazy as _
from .models import LayerSlider, LayerSliderSlides
from guardian.shortcuts import get_objects_for_user
from guardian.admin import GuardedModelAdmin


class LayerSliderSlidesAdmin(admin.StackedInline):
    model = LayerSliderSlides
    template = 'admin/layerslider/layerslider/inclusion/slides_inline.html'
    extra = 0


class LayerSliderAdmin(GuardedModelAdmin):
    model = LayerSlider
    inlines = [LayerSliderSlidesAdmin, ]
    change_form_template = \
        'admin/layerslider/layerslider/change_form.html'
    suit_form_tabs = (
        (u'general', _(u'Основные')),
        (u'slides', _(u'Слайды')),
        (u'slider_preview', _(u'Предосмотр')),
        (u'pro', _(u'Продвинутые')),
    )
    model = LayerSlider
    list_display = ('name', 'created')
    fieldsets = [
        (None, {
            'classes': ('suit-tab', 'suit-tab-general',),
            'fields': ['name', 'slider_width', 'slider_height',
                       'responsive', 'responsiveUnder', 'sublayerContainer',
                       'firstLayer', 'twoWaySlideshow', 'randomSlideshow',
                       'keybNav', 'touchNav', 'imgPreload', 'navPrevNext',
                       'navStartStop', 'navButtons', 'thumbnailNavigation',
                       'tnWidth', 'tnHeight', 'tnContainerWidth', 'tnActiveOpacity',
                       'tnInactiveOpacity', 'skin', 'pauseOnHover']
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-pro',),
            'fields': ['configuration', 'slider_code']
        }),
    ]

    suit_form_includes = (
        (
            'admin/layerslider/layerslider/inclusion/slides_edit_tab.html',
            'top',
            'slides'
        ),
        (
            'admin/layerslider/layerslider/inclusion/slider_preview_tab.html',
            'bottom',
            'slider_preview'
        ),
    )

    def get_queryset(self, request):
        return get_objects_for_user(request.user, 'layerslider.view_layerslider')


admin.site.register(LayerSlider, LayerSliderAdmin)