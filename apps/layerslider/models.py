# -*- coding: utf-8 -*-
from django.db import models
from django.utils.translation import ugettext_lazy as _

THUMBNAIL_NAVIGATION_CHOICES = (
    (u'disabled', u'Disabled'),
    (u'hover', u'Hover'),
    (u'always', u'Always')
)

SKIN_CHOICES = (
    (u'glass', u'glass'),
    (u'v5', u'v5'),
)


class LayerSlider(models.Model):
    name = models.CharField(_(u'Название слайдера'), max_length=255)
    configuration = models.TextField(_(u'Конфигурация слайдера'))
    slider_code = models.TextField(_(u'HTML код слайдера'))
    created = models.DateTimeField(u'Дата создания', auto_now_add=True)

    #slider
    slider_width = models.CharField(_(u'Width'), max_length=255, default=u'100%',
                                    help_text=_(u'The width of the slider in pixels. '
                                                u'Accepts percents, but is only recommended for full-width '
                                                u'layout.'))
    slider_height = models.CharField(_(u'Height'), max_length=255, default=u'300',
                                     help_text=_(u'The height of the slider in pixels.'))
    autoStart = models.BooleanField(_(u'Auto start'), default=True)
    responsive = models.BooleanField(_(u'Responsive'), default=False,
                                     help_text=_(u'Responsive mode provides optimal '
                                                 u'viewing experience across a wide range'
                                                 u' of devices (from desktop to mobile)'
                                                 u' by adapting and scaling your sliders '
                                                 u'for the viewing environment.'))
    responsiveUnder = models.PositiveIntegerField(_(u'Responsive under'), default=0)
    sublayerContainer = models.PositiveIntegerField(_(u'Layers container'), default=0)
    firstLayer = models.PositiveIntegerField(_(u'First layer'), default=1)
    twoWaySlideshow = models.BooleanField(_(u'Two way slide show'), default=False)
    randomSlideshow = models.BooleanField(_(u'Shuffle mode'), default=False)
    keybNav = models.BooleanField(_(u'Keyboard navigation'), default=True)
    touchNav = models.BooleanField(_(u'Touch navigation'), default=True)
    imgPreload = models.BooleanField(_(u'Image preload'), default=True)
    navPrevNext = models.BooleanField(_(u'Show Prev & Next buttons'), default=True)
    navStartStop = models.BooleanField(_(u'Show Start & Stop buttons'), default=True)
    navButtons = models.BooleanField(_(u'Show slide navigation buttons'), default=True)
    thumbnailNavigation = models.CharField(_(u'Thumbnail navigation'), max_length=255, default=u'hover', choices=THUMBNAIL_NAVIGATION_CHOICES)
    tnWidth = models.CharField(_(u'Thumbnail width'), max_length=255, default=u'100')
    tnHeight = models.CharField(_(u'Thumbnail height'), max_length=255, default=u'60')
    tnContainerWidth = models.CharField(_(u'Thumbnail container width'), max_length=255, default=u'60%')
    tnActiveOpacity = models.PositiveIntegerField(_(u'Active opacity'), default=u'100')
    tnInactiveOpacity = models.PositiveIntegerField(_(u'Inactive opacity'), default=u'100')
    skin = models.CharField(_(u'Skin'), max_length=255, default=u'/static/layerslider/css/skins/', choices=SKIN_CHOICES)
    pauseOnHover = models.BooleanField(_(u'Pause OnHover'), default=True)

    class Meta:
        verbose_name = _(u'Слайдер')
        verbose_name_plural = _(u'Слайдеры')
        permissions = (
            ('view_layerslider', 'Can view layerslider'),
        )

    def __unicode__(self):
        return u'Слайдер - %s' % self.name


class LayerSliderSlides(models.Model):
    slider = models.ForeignKey('LayerSlider')
    image_url = models.CharField(_(u'Slide image'), max_length=255)
    duration = models.PositiveIntegerField(_(u'Duration'), default=4000, help_text=_(u'ms'))
    transition = models.CharField(_(u'Transition'), max_length=255, default=u'1')
    time_shift = models.IntegerField(_(u'Time Shift'), default=0,
                                     help_text=_(u'You can control here the timing'
                                                 u' of the layer animations when'
                                                 u' the slider changes to this slide'
                                                 u' with a 3D/2D transition.'
                                                 u' Zero means that the layers of '
                                                 u'this slide will animate in when '
                                                 u'the slide transition ends. '
                                                 u'You can time-shift the starting time'
                                                 u' of the layer animations with positive '
                                                 u'or negative values.'))

    class Meta:
        verbose_name = _(u'Слайд')
        verbose_name_plural = _(u'Слайд')

    def __unicode__(self):
        return u'Слайд - %s' % self.id