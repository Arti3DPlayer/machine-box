# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='LayerSlider',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=255, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0441\u043b\u0430\u0439\u0434\u0435\u0440\u0430')),
                ('configuration', models.TextField(verbose_name='\u041a\u043e\u043d\u0444\u0438\u0433\u0443\u0440\u0430\u0446\u0438\u044f \u0441\u043b\u0430\u0439\u0434\u0435\u0440\u0430')),
                ('slider_code', models.TextField(verbose_name='HTML \u043a\u043e\u0434 \u0441\u043b\u0430\u0439\u0434\u0435\u0440\u0430')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('slider_width', models.CharField(default='100%', help_text='The width of the slider in pixels. Accepts percents, but is only recommended for full-width layout.', max_length=255, verbose_name='Width')),
                ('slider_height', models.CharField(default='300', help_text='The height of the slider in pixels.', max_length=255, verbose_name='Height')),
                ('autoStart', models.BooleanField(default=True, verbose_name='Auto start')),
                ('responsive', models.BooleanField(default=False, help_text='Responsive mode provides optimal viewing experience across a wide range of devices (from desktop to mobile) by adapting and scaling your sliders for the viewing environment.', verbose_name='Responsive')),
                ('responsiveUnder', models.PositiveIntegerField(default=0, verbose_name='Responsive under')),
                ('sublayerContainer', models.PositiveIntegerField(default=0, verbose_name='Layers container')),
                ('firstLayer', models.PositiveIntegerField(default=1, verbose_name='First layer')),
                ('twoWaySlideshow', models.BooleanField(default=False, verbose_name='Two way slide show')),
                ('randomSlideshow', models.BooleanField(default=False, verbose_name='Shuffle mode')),
                ('keybNav', models.BooleanField(default=True, verbose_name='Keyboard navigation')),
                ('touchNav', models.BooleanField(default=True, verbose_name='Touch navigation')),
                ('imgPreload', models.BooleanField(default=True, verbose_name='Image preload')),
                ('navPrevNext', models.BooleanField(default=True, verbose_name='Show Prev & Next buttons')),
                ('navStartStop', models.BooleanField(default=True, verbose_name='Show Start & Stop buttons')),
                ('navButtons', models.BooleanField(default=True, verbose_name='Show slide navigation buttons')),
                ('thumbnailNavigation', models.CharField(default='hover', max_length=255, verbose_name='Thumbnail navigation', choices=[('disabled', 'Disabled'), ('hover', 'Hover'), ('always', 'Always')])),
                ('tnWidth', models.CharField(default='100', max_length=255, verbose_name='Thumbnail width')),
                ('tnHeight', models.CharField(default='60', max_length=255, verbose_name='Thumbnail height')),
                ('tnContainerWidth', models.CharField(default='60%', max_length=255, verbose_name='Thumbnail container width')),
                ('tnActiveOpacity', models.PositiveIntegerField(default='100', verbose_name='Active opacity')),
                ('tnInactiveOpacity', models.PositiveIntegerField(default='100', verbose_name='Inactive opacity')),
                ('skin', models.CharField(default='/static/layerslider/css/skins/', max_length=255, verbose_name='Skin', choices=[('glass', 'glass'), ('v5', 'v5')])),
                ('pauseOnHover', models.BooleanField(default=True, verbose_name='Pause OnHover')),
            ],
            options={
                'verbose_name': '\u0421\u043b\u0430\u0439\u0434\u0435\u0440',
                'verbose_name_plural': '\u0421\u043b\u0430\u0439\u0434\u0435\u0440\u044b',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='LayerSliderSlides',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image_url', models.CharField(max_length=255, verbose_name='Slide image')),
                ('duration', models.PositiveIntegerField(default=4000, help_text='ms', verbose_name='Duration')),
                ('transition', models.CharField(default='1', max_length=255, verbose_name='Transition')),
                ('time_shift', models.IntegerField(default=0, help_text='You can control here the timing of the layer animations when the slider changes to this slide with a 3D/2D transition. Zero means that the layers of this slide will animate in when the slide transition ends. You can time-shift the starting time of the layer animations with positive or negative values.', verbose_name='Time Shift')),
                ('slider', models.ForeignKey(to='layerslider.LayerSlider')),
            ],
            options={
                'verbose_name': '\u0421\u043b\u0430\u0439\u0434',
                'verbose_name_plural': '\u0421\u043b\u0430\u0439\u0434',
            },
            bases=(models.Model,),
        ),
    ]
