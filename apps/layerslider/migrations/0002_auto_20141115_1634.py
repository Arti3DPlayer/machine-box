# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('layerslider', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='layerslider',
            options={'verbose_name': '\u0421\u043b\u0430\u0439\u0434\u0435\u0440', 'verbose_name_plural': '\u0421\u043b\u0430\u0439\u0434\u0435\u0440\u044b', 'permissions': (('view_layerslider', 'Can view layerslider'),)},
        ),
    ]
