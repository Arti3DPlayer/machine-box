# -*- coding: utf-8 -*-
from django import template
from django.core.cache import cache
from django.utils.translation import ugettext_lazy as _

from apps.layerslider.models import LayerSlider

register = template.Library()

@register.inclusion_tag('admin/layerslider/layerslider/inclusion/layerslider.html')
def layerslider_tag(pk):
    try:
        layerslider = LayerSlider.objects.get(pk=pk)
    except LayerSlider.DoesNotExist:
        layerslider = None
    return {
        'layerslider': layerslider
        }


@register.inclusion_tag('admin/layerslider/layerslider/inclusion/ls_scripts.html')
def layerslider_scripts_tag():
    return {}