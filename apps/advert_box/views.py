# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.views.generic import TemplateView, ListView, DetailView, View
from apps.profile.utils import AdvertBoxMachineRequiredMixin


class AdvertBoxMachineView(AdvertBoxMachineRequiredMixin, TemplateView):
    template_name = 'advert_box/home.html'
