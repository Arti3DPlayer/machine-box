# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from apps.advert_box.views import AdvertBoxMachineView

urlpatterns = patterns('',
    url(r'$', AdvertBoxMachineView.as_view(), name='home'),
)