# -*- encoding: utf-8 -*-
from django import template
from django.db.models.query import ValuesListQuerySet
from apps.profile.models import MusicBoxMachine
from guardian.shortcuts import get_objects_for_user

register = template.Library()
