# -*- encoding: utf-8 -*-
from django.contrib import admin

from apps.profile.models import MusicBoxMachine
from guardian.shortcuts import get_objects_for_user


class CustomAdminSite(admin.AdminSite):
    def index(self, request, extra_context=None):
        if extra_context is None:
            extra_context = {}
        extra_context["addresses"] = get_objects_for_user(request.user, 'profile.view_music_box').values_list('address')
        return super(CustomAdminSite, self).index(request, extra_context)


admin.site = CustomAdminSite()
