# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url
from apps.main.views import HomeView


urlpatterns = patterns('',
    url(r'^$', HomeView.as_view(), name='home'),
)