from django.shortcuts import render
from django.views.generic import TemplateView, ListView, DetailView, View, RedirectView
from django.core.urlresolvers import reverse_lazy


class HomeView(RedirectView):
    url = reverse_lazy('profile:sign_in')
