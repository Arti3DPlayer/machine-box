# -*- encoding: utf-8 -*-
from django import forms
from django.contrib.auth.forms import UserChangeForm, UserCreationForm
from apps.profile.models import User, MusicBoxMachine, AdvertMachine


class UserAdminForm(UserChangeForm):

    class Meta(UserChangeForm.Meta):
        model = User


class UserAdminCreationForm(UserCreationForm):

    class Meta(UserCreationForm.Meta):
        model = User

    def clean_username(self):
        username = self.cleaned_data["username"]
        try:
            User._default_manager.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(
            self.error_messages['duplicate_username'],
            code='duplicate_username',)


class MusicBoxMachineAdminCreationForm(UserCreationForm):

    class Meta(UserCreationForm.Meta):
        model = MusicBoxMachine

    def clean_username(self):
        username = self.cleaned_data["username"]
        try:
            MusicBoxMachine._default_manager.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(
            self.error_messages['duplicate_username'],
            code='duplicate_username',)


class AdvertMachineAdminCreationForm(UserCreationForm):

    class Meta(UserCreationForm.Meta):
        model = AdvertMachine

    def clean_username(self):
        username = self.cleaned_data["username"]
        try:
            AdvertMachine._default_manager.get(username=username)
        except User.DoesNotExist:
            return username
        raise forms.ValidationError(
            self.error_messages['duplicate_username'],
            code='duplicate_username',)