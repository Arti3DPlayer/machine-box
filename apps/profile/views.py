# -*- coding: utf-8 -*-
import time
import hashlib
import json
import decimal

from django.shortcuts import render

from django.views.generic import FormView, View, TemplateView
from apps.profile.utils import GuestRequiredMixin
from django.utils import timezone
from django.core.urlresolvers import reverse_lazy
from django.contrib import auth, messages
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django_socketio import broadcast, broadcast_channel, NoSocket
from django.template.loader import render_to_string
from django.shortcuts import get_object_or_404

from django.contrib.auth.forms import AuthenticationForm
from apps.profile.utils import MusicBoxMachineRequiredMixin, get_client_ip
from apps.profile.models import User, MusicBoxMachine, KeyStorage


class SignInView(FormView):
    template_name = 'profile/sign_in.html'
    form_class = AuthenticationForm
    success_url = reverse_lazy('profile:sign_in')

    def get_success_url(self):
        if self.request.GET.get('next'):
            self.success_url = self.request.GET.get('next')
        elif self.request.user.is_user():
            self.success_url = reverse_lazy('admin:login')
        elif self.request.user.is_music_box_machine():
            self.success_url = reverse_lazy('music_box:genre_list')
        elif self.request.user.is_advert_box_machine():
            self.success_url = reverse_lazy('advert_box:home')
        return self.success_url

    def form_valid(self, form):
        cd = form.cleaned_data
        user = auth.authenticate(username=cd['username'], password=cd['password'])
        auth.login(self.request, user)

        try:
            self.request.user = self.request.user.musicboxmachine
        except User.DoesNotExist:
            pass
        try:
            self.request.user = self.request.user.advertmachine
        except User.DoesNotExist:
            pass

        return super(SignInView, self).form_valid(form)


class KeyStorageCreateView(View):
    #TODO зачем это я так и не понял, спросить у никиты
    def get(self, request, *args, **kwargs):
        data = request.GET
        if 'key' not in data or 'secret_key' not in data:
            raise Http404

        try:
            KeyStorage.objects.get(key=data['key'])
        except KeyStorage.DoesNotExist:
            obj = KeyStorage(key=data['key'])
            obj.secret_key = data['secret_key']
            obj.ip = get_client_ip(request)
            obj.save()

        return HttpResponse(u'1', content_type='text/plain')