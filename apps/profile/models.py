# -*- coding: utf-8 -*-
from django.db import models
from django.core import validators
from django.utils import timezone
from django.contrib.auth.models import AbstractUser
from django.contrib.auth.models import BaseUserManager
from django.db.models import signals

from apps.layerslider.models import LayerSlider


PAYMENT_TYPE = (
    ('refill', u'Пополнение счета'),
    ('give_change', u'Выдача сдачи'),
    ('buy_track', u'Покупка трека')
)

SERVICE_CHOISES = (
    ('vkontakte', u'Вконтакте'),
    ('soundcloud', u'SoundCloud')
)


class UserManager(BaseUserManager):

    def _create_user(self, username, password,
                     is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """
        now = timezone.now()
        user = self.model(username=username, is_staff=is_staff,
                          is_active=True, is_superuser=is_superuser,
                          last_login=now, date_joined=now, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, password=None, **extra_fields):
        return self._create_user(username, password, False, False,
                                 **extra_fields)

    def create_superuser(self, username, password, **extra_fields):
        return self._create_user(username, password, True, True,
                                 **extra_fields)


class User(AbstractUser):

    objects = UserManager()

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = u'Пользователь'
        verbose_name_plural = u'Пользователи'

    def is_user(self):
        if type(self) is User:
            return True
        return False

    def is_music_box_machine(self):
        if type(self) is MusicBoxMachine:
            return True
        return False

    def is_advert_box_machine(self):
        if type(self) is AdvertMachine:
            return True
        return False


class MusicBoxMachine(User):
    lastfm_url = models.CharField(u'URL', max_length=255, default=u'http://ws.audioscrobbler.com/2.0/')
    lastfm_api_key = models.CharField(u'API_KEY', max_length=255)
    lastfm_api_secret = models.CharField(u'API_SECRET', max_length=255)
    vk_login = models.CharField(u'Phone number', max_length=255, blank=True)
    vk_password = models.CharField(u'Password', max_length=255, blank=True)
    vk_app_id = models.CharField(u'App ID', max_length=255, default=u'4497456')
    soundcloud_client_id = models.CharField(u'Client ID', max_length=255, blank=True)
    soundcloud_client_secret = models.CharField(u'Client Secret', max_length=255, blank=True)
    service = models.CharField(u'Сервис для получения музыки', choices=SERVICE_CHOISES, max_length=255, default='vkontakte')
    address = models.CharField(u'Адресс автомата', max_length=255, blank=True, help_text=u'Место расположения автомата')
    key_machine = models.CharField(u'Key', max_length=255)
    secret_key_machine = models.CharField(u'Secret key', max_length=255)
    cash = models.DecimalField(u'Текущий баланс', max_digits=12, decimal_places=2,
                               default=0)
    hopper_enabled = models.BooleanField(u'Хоппер включён', default=False,
                                         help_text=u'Может ли автомат выдавать сдачу?')
    hopper_nominal = models.IntegerField(u'Номинал монет в хоппере', default=5)
    hopper_cash = models.DecimalField(u'Баланс хоппера', max_digits=12, decimal_places=2, default=0,
                                      help_text=u'Значение не обновляется автоматически.')
    credit_cash = models.DecimalField(u'Кредиты', max_digits=12, decimal_places=2,
                                      default=0, help_text=u'Временный баланс, предназначеный для выдачи сдачи.')
    give_change_confirm = models.BooleanField(u'Флаг для разрешения выдачи сдачи', default=False)
    price = models.DecimalField(u'Цена за песню', max_digits=12, decimal_places=2,
                                default=0)
    delay_before_playing_random_music = models.PositiveIntegerField(u'Задержка перед проигрыванием случайной музыки',
                                                                    default=5, help_text=u'Время в секундах.')
    delay_before_starting_advertising = models.PositiveIntegerField(u'Задержка перед показом рекламы.',
                                                                    default=5, help_text=u'Время в секундах.')
    delay_before_playing_music = models.PositiveIntegerField(u'Задержка перед проигрыванием купленой музыки.',
                                                             default=5, help_text=u'Время в секундах.')
    advert_sound_file = models.FileField(u'Рекламный звуковой файл', upload_to='music_box', null=True, blank=True)
    delay_before_advert_sound_file = models.PositiveIntegerField(u'Количество треков для проигрывания звуковой рекламы.',
                                                                 default=5)
    update_cash_message = models.TextField(u'Сообщение о переносе суммы в кредит или выдачи сдачи.', blank=True)
    success_text = models.TextField(u'Текст после успешной покупки музыки', blank=True)

    theme_file = models.FileField(u'CSS файл', upload_to='music_box',
                                  help_text=u'''
                                    Необходимо перезагрузить страницу и
                                    сбросить кеш браузера после загрузки файла.
                                  ''',
                                  null=True, blank=True)
    advert_slider = models.ForeignKey(LayerSlider, verbose_name=u'Рекламный слайдер', null=True, blank=True)

    class Meta:
        verbose_name = u'Музыкальный автомат'
        verbose_name_plural = u'Музыкальные автоматы'
        permissions = (
            ('view_music_box', 'Can view music box'),
        )

    def __unicode__(self):
        return u'Музыкальный автомат -  %s' % self.username


class AdvertMachine(User):
    address = models.CharField(u'Адресс автомата', max_length=255, blank=True, help_text=u'Место расположения автомата')
    advert_slider = models.ForeignKey(LayerSlider, verbose_name=u'Рекламный слайдер', null=True)

    class Meta:
        verbose_name = u'Рекламный автомат'
        verbose_name_plural = u'Рекламные автоматы'
        permissions = (
            ('view_advert_box', 'Can view advert box'),
        )

    def __unicode__(self):
        return u'Рекламный автомат -  %s' % self.username


class KeyStorage(models.Model):
    key = models.CharField(
        u'Ключ', max_length=64, unique=True,
        help_text=u'Уникальный индификатор терминала')
    secret_key = models.CharField(u'Секретный ключ', max_length=64)
    ip = models.CharField(u'IP', max_length=64)
    created = models.DateTimeField(u'Дата создания', default=timezone.now)
    is_create_terminal = models.BooleanField(
        u'Терминал создан', default=False)

    class Meta:
        verbose_name = u'ключи'
        verbose_name_plural = u'ключи'

    def __unicode__(self):
        return unicode(self.key)


def check_key(sender, instance, created, *args, **kwargs):
    if created:
        try:
            storage = KeyStorage.objects.get(key=instance.key_machine)
        except KeyStorage.DoesNotExist:
            return

        storage.is_create_terminal = True
        storage.save()


def del_key(sender, instance, *args, **kwargs):
    try:
        storage = KeyStorage.objects.get(key=instance.key_machine)
    except KeyStorage.DoesNotExist:
        return

    storage.is_create_terminal = False
    storage.save()

signals.post_save.connect(check_key, sender=MusicBoxMachine)
signals.pre_delete.connect(del_key, sender=MusicBoxMachine)


class Payment(models.Model):
    machine = models.ForeignKey(
        'MusicBoxMachine', related_name='payment', verbose_name=u'Автомат')
    description = models.TextField(u'Описание платежа')
    payment_type = models.CharField(
        u'Тип платежа', max_length=30,
        choices=PAYMENT_TYPE, null=True, blank=True)
    price = models.DecimalField(
        u'Цена', max_digits=12, decimal_places=2, default=0)
    created = models.DateTimeField(u'Дата создания', auto_now_add=True)

    def __unicode__(self):
        return unicode(u'№%s - %s' % (self.pk, self.machine.username))

    class Meta:
        verbose_name = u'Платеж'
        verbose_name_plural = u'Платежи'