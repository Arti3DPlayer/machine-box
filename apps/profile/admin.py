# -*- encoding: utf-8 -*-
from django.contrib import admin
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

from apps.profile.models import User, MusicBoxMachine, AdvertMachine, Payment, KeyStorage
from apps.profile.forms import UserAdminCreationForm, UserAdminForm, MusicBoxMachineAdminCreationForm, \
    AdvertMachineAdminCreationForm


from guardian.shortcuts import get_objects_for_user
from guardian.admin import GuardedModelAdmin


class UserAdmin(BaseUserAdmin):
    form = UserAdminForm
    add_form = UserAdminCreationForm

    def get_queryset(self, request):
        """
            Исключаем автоматы из списка юзеров
        """
        qs = super(UserAdmin, self).get_queryset(request)
        return qs.filter(musicboxmachine=None, advertmachine=None)


class MusicBoxMachineAdmin(GuardedModelAdmin, BaseUserAdmin):
    model = MusicBoxMachine
    add_form = MusicBoxMachineAdminCreationForm
    fieldsets = (
        (u'Автомат', {'fields': (
            'username', 'password', 'address', 'key_machine', 'secret_key_machine',
            'service', 'hopper_enabled', 'hopper_nominal', 'hopper_cash',
        )}),
        (u'Настройки', {'fields': ('cash', 'price', 'delay_before_playing_random_music',
                                   'advert_slider',
                                   'delay_before_starting_advertising', 'delay_before_playing_music',
                                   'advert_sound_file', 'delay_before_advert_sound_file', 'success_text',
                                   'update_cash_message', 'theme_file')}),
        (u'Last.fm', {'fields': (
            'lastfm_url', 'lastfm_api_key', 'lastfm_api_secret',
        )}),
        (u'Вконтакте', {'fields': (
            'vk_login', 'vk_password', 'vk_app_id'
        )}),
        (u'SoundCloud', {'fields': (
            'soundcloud_client_id', 'soundcloud_client_secret',
        )}),
        (u'Важные даты', {'fields': ('last_login', 'date_joined')}),
    )

    list_display = ('username', 'price', 'last_login', 'is_active')
    list_filter = ('is_active', )
    search_fields = ('username', )
    ordering = ('username',)

    def get_queryset(self, request):
        return get_objects_for_user(request.user, 'profile.view_music_box')


class AdvertMachineAdmin(GuardedModelAdmin, BaseUserAdmin):
    model = AdvertMachine
    add_form = AdvertMachineAdminCreationForm
    fieldsets = (
        (u'Автомат', {'fields': (
            'username', 'password', 'address', 'is_active',
        )}),
        (u'Настройки', {'fields': ('advert_slider',)}),
        (u'Важные даты', {'fields': ('last_login', 'date_joined')}),
    )

    list_display = ('username', 'last_login', 'is_active')
    list_filter = ('is_active', )
    search_fields = ('username', )
    ordering = ('username',)

    def get_queryset(self, request):
        return get_objects_for_user(request.user, 'profile.view_advert_box')


class PaymentAdmin(admin.ModelAdmin):
    model = Payment
    list_display = ('machine', 'payment_type', 'price', 'created')
    list_filter = ('machine__username', 'payment_type')
    readonly_fields = ('machine', 'payment_type', 'price', 'created', 'description')

    def has_add_permission(self, request):
        return False

    #TODO доделать
    # def get_queryset(self, request):
    #     ids = get_objects_for_user(request.user, 'profile.view_advert_box').values('id')
    #     return self.model.objects.filter(id__in = ids)


class KeyStorageAdmin(admin.ModelAdmin):
    list_display = ('key', 'secret_key', 'ip', 'created',
                    'is_create_terminal', 'machine_create_link')
    list_display_links = tuple()
    search_fields = ('key', 'ip')
    list_filter = ('is_create_terminal',)

    def machine_create_link(self, instance):
        if instance.is_create_terminal:
            return u'Уже создан'
        url = reverse_lazy('admin:profile_machine_add')
        return u'<a href="%s?key=%s&secret_key=%s">Создать автомат</a>' % (
            url, instance.key, instance.secret_key)
    machine_create_link.allow_tags = True
    machine_create_link.short_description = u'Дейсвтие'

    def has_add_permission(self, request):
        return False

    def has_change_permission(self, request, obj=None):
        return not obj

admin.site.register(User, UserAdmin)
admin.site.register(MusicBoxMachine, MusicBoxMachineAdmin)
admin.site.register(AdvertMachine, AdvertMachineAdmin)
admin.site.register(Payment, PaymentAdmin)
admin.site.register(KeyStorage, KeyStorageAdmin)
