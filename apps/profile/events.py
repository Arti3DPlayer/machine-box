# -*- coding: utf-8 -*-
import decimal

from django.shortcuts import get_object_or_404
from django.utils.html import strip_tags
from django.contrib import auth, messages
from django_socketio.events import on_message, on_connect
from apps.profile.models import User, MusicBoxMachine


@on_message
def message(request, socket, context, message):
    print message
    name = message.get('username')
    password = message.get('password')
    amount = message.get('amount')

    if name and password and amount:
        user = auth.authenticate(username=name, password=password)
        if user:
            machine = MusicBoxMachine.objects.get(user_ptr_id=user.id)
            if machine:
                machine.cash += decimal.Decimal(amount)
                machine.save()
                data = {'action': 'update_cash', 'cash': float(machine.cash)}
                socket.broadcast_channel(data, "machine-"+str(machine.id))
        else:
            print u"Auth failed."
            return
    else:
        print u"Not enough args. Username, password and amount are required."
        return