# -*- encoding: utf-8 -*-
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy


class LoginRequiredMixin(object):
    """
    Ensures that user must be authenticated in order to access view.
    """

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(
            request, *args, **kwargs)


class GuestRequiredMixin(object):
    """Доступ к станице только гостям(не авторизированным юзерам)"""

    def dispatch(self, request, *args, **kwargs):
        if request.user.is_authenticated():
            return HttpResponseRedirect(
                reverse_lazy('music_box:home'))
        return super(GuestRequiredMixin, self).dispatch(
            request, *args, **kwargs)


class MusicBoxMachineRequiredMixin(object):
    """Доступ к странице только музыкальному автомату"""

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated() or not request.user.is_music_box_machine():
            return HttpResponseRedirect(reverse_lazy('profile:sign_in'))
        return super(MusicBoxMachineRequiredMixin, self).dispatch(
            request, *args, **kwargs)


class AdvertBoxMachineRequiredMixin(object):
    """Доступ к странице только рекламному автомату"""

    def dispatch(self, request, *args, **kwargs):
        if not request.user.is_authenticated() or not request.user.is_advert_box_machine():
            return HttpResponseRedirect(reverse_lazy('profile:sign_in'))
        return super(AdvertBoxMachineRequiredMixin, self).dispatch(
            request, *args, **kwargs)


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    # Заглушка для локалки ip Google
    if ip in ('127.0.0.1', 'localhost'):
        ip = '74.125.77.147'
    return ip