# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0003_user_access_users'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='musicboxmachine',
            options={'verbose_name': '\u041c\u0443\u0437\u044b\u043a\u0430\u043b\u044c\u043d\u044b\u0439 \u0430\u0432\u0442\u043e\u043c\u0430\u0442', 'verbose_name_plural': '\u041c\u0443\u0437\u044b\u043a\u0430\u043b\u044c\u043d\u044b\u0435 \u0430\u0432\u0442\u043e\u043c\u0430\u0442\u044b', 'permissions': (('view_music_box', 'Can view music box'),)},
        ),
        migrations.AlterField(
            model_name='user',
            name='access_users',
            field=models.ManyToManyField(related_name='access_users_rel_+', null=True, verbose_name='\u0414\u043e\u0441\u0442\u0443\u043f \u043a \u043e\u0431\u044c\u0435\u043a\u0442\u0443', to=settings.AUTH_USER_MODEL, blank=True),
            preserve_default=True,
        ),
    ]
