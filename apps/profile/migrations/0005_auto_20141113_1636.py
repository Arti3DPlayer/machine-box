# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0004_auto_20141113_1617'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='advertmachine',
            options={'verbose_name': '\u0420\u0435\u043a\u043b\u0430\u043c\u043d\u044b\u0439 \u0430\u0432\u0442\u043e\u043c\u0430\u0442', 'verbose_name_plural': '\u0420\u0435\u043a\u043b\u0430\u043c\u043d\u044b\u0435 \u0430\u0432\u0442\u043e\u043c\u0430\u0442\u044b', 'permissions': (('view_advert_box', 'Can view advert box'),)},
        ),
        migrations.RemoveField(
            model_name='user',
            name='access_users',
        ),
    ]
