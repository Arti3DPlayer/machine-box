# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('auth', '0001_initial'),
        ('layerslider', '__first__'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(default=django.utils.timezone.now, verbose_name='last login')),
                ('is_superuser', models.BooleanField(default=False, help_text='Designates that this user has all permissions without explicitly assigning them.', verbose_name='superuser status')),
                ('username', models.CharField(help_text='Required. 30 characters or fewer. Letters, digits and @/./+/-/_ only.', unique=True, max_length=30, verbose_name='username', validators=[django.core.validators.RegexValidator('^[\\w.@+-]+$', 'Enter a valid username.', 'invalid')])),
                ('first_name', models.CharField(max_length=30, verbose_name='first name', blank=True)),
                ('last_name', models.CharField(max_length=30, verbose_name='last name', blank=True)),
                ('email', models.EmailField(max_length=75, verbose_name='email address', blank=True)),
                ('is_staff', models.BooleanField(default=False, help_text='Designates whether the user can log into this admin site.', verbose_name='staff status')),
                ('is_active', models.BooleanField(default=True, help_text='Designates whether this user should be treated as active. Unselect this instead of deleting accounts.', verbose_name='active')),
                ('date_joined', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date joined')),
            ],
            options={
                'verbose_name': '\u041f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u044c',
                'verbose_name_plural': '\u041f\u043e\u043b\u044c\u0437\u043e\u0432\u0430\u0442\u0435\u043b\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='AdvertiseMachine',
            fields=[
                ('user_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('address', models.CharField(max_length=255, verbose_name='\u0410\u0434\u0440\u0435\u0441\u0441 \u0430\u0432\u0442\u043e\u043c\u0430\u0442\u0430', blank=True)),
                ('advert_slider', models.ForeignKey(verbose_name='\u0420\u0435\u043a\u043b\u0430\u043c\u043d\u044b\u0439 \u0441\u043b\u0430\u0439\u0434\u0435\u0440', to='layerslider.LayerSlider', null=True)),
            ],
            options={
                'verbose_name': '\u0420\u0435\u043a\u043b\u0430\u043c\u043d\u044b\u0439 \u0430\u0432\u0442\u043e\u043c\u0430\u0442',
                'verbose_name_plural': '\u0420\u0435\u043a\u043b\u0430\u043c\u043d\u044b\u0435 \u0430\u0432\u0442\u043e\u043c\u0430\u0442\u044b',
            },
            bases=('profile.user',),
        ),
        migrations.CreateModel(
            name='KeyStorage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('key', models.CharField(help_text='\u0423\u043d\u0438\u043a\u0430\u043b\u044c\u043d\u044b\u0439 \u0438\u043d\u0434\u0438\u0444\u0438\u043a\u0430\u0442\u043e\u0440 \u0442\u0435\u0440\u043c\u0438\u043d\u0430\u043b\u0430', unique=True, max_length=64, verbose_name='\u041a\u043b\u044e\u0447')),
                ('secret_key', models.CharField(max_length=64, verbose_name='\u0421\u0435\u043a\u0440\u0435\u0442\u043d\u044b\u0439 \u043a\u043b\u044e\u0447')),
                ('ip', models.CharField(max_length=64, verbose_name='IP')),
                ('created', models.DateTimeField(default=django.utils.timezone.now, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('is_create_terminal', models.BooleanField(default=False, verbose_name='\u0422\u0435\u0440\u043c\u0438\u043d\u0430\u043b \u0441\u043e\u0437\u0434\u0430\u043d')),
            ],
            options={
                'verbose_name': '\u043a\u043b\u044e\u0447\u0438',
                'verbose_name_plural': '\u043a\u043b\u044e\u0447\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='MusicBoxMachine',
            fields=[
                ('user_ptr', models.OneToOneField(parent_link=True, auto_created=True, primary_key=True, serialize=False, to=settings.AUTH_USER_MODEL)),
                ('lastfm_url', models.CharField(default='http://ws.audioscrobbler.com/2.0/', max_length=255, verbose_name='URL')),
                ('lastfm_api_key', models.CharField(max_length=255, verbose_name='API_KEY')),
                ('lastfm_api_secret', models.CharField(max_length=255, verbose_name='API_SECRET')),
                ('vk_login', models.CharField(max_length=255, verbose_name='Phone number', blank=True)),
                ('vk_password', models.CharField(max_length=255, verbose_name='Password', blank=True)),
                ('vk_app_id', models.CharField(default='4497456', max_length=255, verbose_name='App ID')),
                ('soundcloud_client_id', models.CharField(max_length=255, verbose_name='Client ID', blank=True)),
                ('soundcloud_client_secret', models.CharField(max_length=255, verbose_name='Client Secret', blank=True)),
                ('service', models.CharField(default=b'vkontakte', max_length=255, verbose_name='\u0421\u0435\u0440\u0432\u0438\u0441 \u0434\u043b\u044f \u043f\u043e\u043b\u0443\u0447\u0435\u043d\u0438\u044f \u043c\u0443\u0437\u044b\u043a\u0438', choices=[(b'vkontakte', '\u0412\u043a\u043e\u043d\u0442\u0430\u043a\u0442\u0435'), (b'soundcloud', 'SoundCloud')])),
                ('address', models.CharField(max_length=255, verbose_name='\u0410\u0434\u0440\u0435\u0441\u0441 \u0430\u0432\u0442\u043e\u043c\u0430\u0442\u0430', blank=True)),
                ('key_machine', models.CharField(max_length=255, verbose_name='Key')),
                ('secret_key_machine', models.CharField(max_length=255, verbose_name='Secret key')),
                ('cash', models.DecimalField(default=0, verbose_name='\u0422\u0435\u043a\u0443\u0449\u0438\u0439 \u0431\u0430\u043b\u0430\u043d\u0441', max_digits=12, decimal_places=2)),
                ('hopper_enabled', models.BooleanField(default=False, help_text='\u041c\u043e\u0436\u0435\u0442 \u043b\u0438 \u0430\u0432\u0442\u043e\u043c\u0430\u0442 \u0432\u044b\u0434\u0430\u0432\u0430\u0442\u044c \u0441\u0434\u0430\u0447\u0443?', verbose_name='\u0425\u043e\u043f\u043f\u0435\u0440 \u0432\u043a\u043b\u044e\u0447\u0451\u043d')),
                ('credit_cash', models.DecimalField(default=0, help_text='\u0412\u0440\u0435\u043c\u0435\u043d\u043d\u044b\u0439 \u0431\u0430\u043b\u0430\u043d\u0441, \u043f\u0440\u0435\u0434\u043d\u0430\u0437\u043d\u0430\u0447\u0435\u043d\u044b\u0439 \u0434\u043b\u044f \u0432\u044b\u0434\u0430\u0447\u0438 \u0441\u0434\u0430\u0447\u0438.', verbose_name='\u041a\u0440\u0435\u0434\u0438\u0442\u044b', max_digits=12, decimal_places=2)),
                ('price', models.DecimalField(default=0, verbose_name='\u0426\u0435\u043d\u0430 \u0437\u0430 \u043f\u0435\u0441\u043d\u044e', max_digits=12, decimal_places=2)),
                ('delay_before_playing_random_music', models.PositiveIntegerField(default=5, help_text='\u0412\u0440\u0435\u043c\u044f \u0432 \u0441\u0435\u043a\u0443\u043d\u0434\u0430\u0445.', verbose_name='\u0417\u0430\u0434\u0435\u0440\u0436\u043a\u0430 \u043f\u0435\u0440\u0435\u0434 \u043f\u0440\u043e\u0438\u0433\u0440\u044b\u0432\u0430\u043d\u0438\u0435\u043c \u0441\u043b\u0443\u0447\u0430\u0439\u043d\u043e\u0439 \u043c\u0443\u0437\u044b\u043a\u0438')),
                ('delay_before_starting_advertising', models.PositiveIntegerField(default=5, help_text='\u0412\u0440\u0435\u043c\u044f \u0432 \u0441\u0435\u043a\u0443\u043d\u0434\u0430\u0445.', verbose_name='\u0417\u0430\u0434\u0435\u0440\u0436\u043a\u0430 \u043f\u0435\u0440\u0435\u0434 \u043f\u043e\u043a\u0430\u0437\u043e\u043c \u0440\u0435\u043a\u043b\u0430\u043c\u044b.')),
                ('delay_before_playing_music', models.PositiveIntegerField(default=5, help_text='\u0412\u0440\u0435\u043c\u044f \u0432 \u0441\u0435\u043a\u0443\u043d\u0434\u0430\u0445.', verbose_name='\u0417\u0430\u0434\u0435\u0440\u0436\u043a\u0430 \u043f\u0435\u0440\u0435\u0434 \u043f\u0440\u043e\u0438\u0433\u0440\u044b\u0432\u0430\u043d\u0438\u0435\u043c \u043a\u0443\u043f\u043b\u0435\u043d\u043e\u0439 \u043c\u0443\u0437\u044b\u043a\u0438.')),
                ('advert_sound_file', models.FileField(upload_to=b'music_box', null=True, verbose_name='\u0420\u0435\u043a\u043b\u0430\u043c\u043d\u044b\u0439 \u0437\u0432\u0443\u043a\u043e\u0432\u043e\u0439 \u0444\u0430\u0439\u043b', blank=True)),
                ('delay_before_advert_sound_file', models.PositiveIntegerField(default=5, verbose_name='\u041a\u043e\u043b\u0438\u0447\u0435\u0441\u0442\u0432\u043e \u0442\u0440\u0435\u043a\u043e\u0432 \u0434\u043b\u044f \u043f\u0440\u043e\u0438\u0433\u0440\u044b\u0432\u0430\u043d\u0438\u044f \u0437\u0432\u0443\u043a\u043e\u0432\u043e\u0439 \u0440\u0435\u043a\u043b\u0430\u043c\u044b.')),
                ('update_cash_message', models.TextField(verbose_name='\u0421\u043e\u043e\u0431\u0449\u0435\u043d\u0438\u0435 \u043e \u043f\u0435\u0440\u0435\u043d\u043e\u0441\u0435 \u0441\u0443\u043c\u043c\u044b \u0432 \u043a\u0440\u0435\u0434\u0438\u0442 \u0438\u043b\u0438 \u0432\u044b\u0434\u0430\u0447\u0438 \u0441\u0434\u0430\u0447\u0438.', blank=True)),
                ('success_text', models.TextField(verbose_name='\u0422\u0435\u043a\u0441\u0442 \u043f\u043e\u0441\u043b\u0435 \u0443\u0441\u043f\u0435\u0448\u043d\u043e\u0439 \u043f\u043e\u043a\u0443\u043f\u043a\u0438 \u043c\u0443\u0437\u044b\u043a\u0438', blank=True)),
                ('theme_file', models.FileField(help_text='\n                                    \u041d\u0435\u043e\u0431\u0445\u043e\u0434\u0438\u043c\u043e \u043f\u0435\u0440\u0435\u0437\u0430\u0433\u0440\u0443\u0437\u0438\u0442\u044c \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u0443 \u0438\n                                    \u0441\u0431\u0440\u043e\u0441\u0438\u0442\u044c \u043a\u0435\u0448 \u0431\u0440\u0430\u0443\u0437\u0435\u0440\u0430 \u043f\u043e\u0441\u043b\u0435 \u0437\u0430\u0433\u0440\u0443\u0437\u043a\u0438 \u0444\u0430\u0439\u043b\u0430.\n                                  ', upload_to=b'music_box', null=True, verbose_name='CSS \u0444\u0430\u0439\u043b', blank=True)),
                ('advert_slider', models.ForeignKey(verbose_name='\u0420\u0435\u043a\u043b\u0430\u043c\u043d\u044b\u0439 \u0441\u043b\u0430\u0439\u0434\u0435\u0440', blank=True, to='layerslider.LayerSlider', null=True)),
            ],
            options={
                'verbose_name': '\u041c\u0443\u0437\u044b\u043a\u0430\u043b\u044c\u043d\u044b\u0439 \u0430\u0432\u0442\u043e\u043c\u0430\u0442',
                'verbose_name_plural': '\u041c\u0443\u0437\u044b\u043a\u0430\u043b\u044c\u043d\u044b\u0435 \u0430\u0432\u0442\u043e\u043c\u0430\u0442\u044b',
            },
            bases=('profile.user',),
        ),
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('description', models.TextField(verbose_name='\u041e\u043f\u0438\u0441\u0430\u043d\u0438\u0435 \u043f\u043b\u0430\u0442\u0435\u0436\u0430')),
                ('payment_type', models.CharField(blank=True, max_length=30, null=True, verbose_name='\u0422\u0438\u043f \u043f\u043b\u0430\u0442\u0435\u0436\u0430', choices=[(b'refill', '\u041f\u043e\u043f\u043e\u043b\u043d\u0435\u043d\u0438\u0435 \u0441\u0447\u0435\u0442\u0430'), (b'buy_track', '\u041f\u043e\u043a\u0443\u043f\u043a\u0430 \u0442\u0440\u0435\u043a\u0430')])),
                ('price', models.DecimalField(default=0, verbose_name='\u0426\u0435\u043d\u0430', max_digits=12, decimal_places=2)),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='\u0414\u0430\u0442\u0430 \u0441\u043e\u0437\u0434\u0430\u043d\u0438\u044f')),
                ('machine', models.ForeignKey(related_name='payment', verbose_name='\u0410\u0432\u0442\u043e\u043c\u0430\u0442', to='profile.MusicBoxMachine')),
            ],
            options={
                'verbose_name': '\u041f\u043b\u0430\u0442\u0435\u0436',
                'verbose_name_plural': '\u041f\u043b\u0430\u0442\u0435\u0436\u0438',
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='user',
            name='groups',
            field=models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Group', blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of his/her group.', verbose_name='groups'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='user',
            name='user_permissions',
            field=models.ManyToManyField(related_query_name='user', related_name='user_set', to='auth.Permission', blank=True, help_text='Specific permissions for this user.', verbose_name='user permissions'),
            preserve_default=True,
        ),
    ]
