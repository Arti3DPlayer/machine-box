# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0005_auto_20141113_1636'),
    ]

    operations = [
        migrations.AddField(
            model_name='musicboxmachine',
            name='hopper_cash',
            field=models.DecimalField(default=0, help_text='\u0417\u043d\u0430\u0447\u0435\u043d\u0438\u0435 \u043d\u0435 \u043e\u0431\u043d\u043e\u0432\u043b\u044f\u0435\u0442\u0441\u044f \u0430\u0432\u0442\u043e\u043c\u0430\u0442\u0438\u0447\u0435\u0441\u043a\u0438.', verbose_name='\u0411\u0430\u043b\u0430\u043d\u0441 \u0445\u043e\u043f\u043f\u0435\u0440\u0430', max_digits=12, decimal_places=2),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='musicboxmachine',
            name='hopper_nominal',
            field=models.IntegerField(default=5, verbose_name='\u041d\u043e\u043c\u0438\u043d\u0430\u043b \u043c\u043e\u043d\u0435\u0442 \u0432 \u0445\u043e\u043f\u043f\u0435\u0440\u0435'),
            preserve_default=True,
        ),
    ]
