# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0002_auto_20141112_1438'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='access_users',
            field=models.ManyToManyField(related_name='access_users_rel_+', null=True, to=settings.AUTH_USER_MODEL, blank=True),
            preserve_default=True,
        ),
    ]
