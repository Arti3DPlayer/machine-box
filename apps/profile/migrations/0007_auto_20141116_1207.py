# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('profile', '0006_auto_20141116_0853'),
    ]

    operations = [
        migrations.AddField(
            model_name='musicboxmachine',
            name='give_change_confirm',
            field=models.BooleanField(default=False, verbose_name='\u0424\u043b\u0430\u0433 \u0434\u043b\u044f \u0440\u0430\u0437\u0440\u0435\u0448\u0435\u043d\u0438\u044f \u0432\u044b\u0434\u0430\u0447\u0438 \u0441\u0434\u0430\u0447\u0438'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='advertmachine',
            name='address',
            field=models.CharField(help_text='\u041c\u0435\u0441\u0442\u043e \u0440\u0430\u0441\u043f\u043e\u043b\u043e\u0436\u0435\u043d\u0438\u044f \u0430\u0432\u0442\u043e\u043c\u0430\u0442\u0430', max_length=255, verbose_name='\u0410\u0434\u0440\u0435\u0441\u0441 \u0430\u0432\u0442\u043e\u043c\u0430\u0442\u0430', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='musicboxmachine',
            name='address',
            field=models.CharField(help_text='\u041c\u0435\u0441\u0442\u043e \u0440\u0430\u0441\u043f\u043e\u043b\u043e\u0436\u0435\u043d\u0438\u044f \u0430\u0432\u0442\u043e\u043c\u0430\u0442\u0430', max_length=255, verbose_name='\u0410\u0434\u0440\u0435\u0441\u0441 \u0430\u0432\u0442\u043e\u043c\u0430\u0442\u0430', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='payment',
            name='payment_type',
            field=models.CharField(blank=True, max_length=30, null=True, verbose_name='\u0422\u0438\u043f \u043f\u043b\u0430\u0442\u0435\u0436\u0430', choices=[(b'refill', '\u041f\u043e\u043f\u043e\u043b\u043d\u0435\u043d\u0438\u0435 \u0441\u0447\u0435\u0442\u0430'), (b'give_change', '\u0412\u044b\u0434\u0430\u0447\u0430 \u0441\u0434\u0430\u0447\u0438'), (b'buy_track', '\u041f\u043e\u043a\u0443\u043f\u043a\u0430 \u0442\u0440\u0435\u043a\u0430')]),
            preserve_default=True,
        ),
    ]
