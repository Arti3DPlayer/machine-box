# -*- coding: utf-8 -*-
from django.conf.urls import patterns, url

from apps.profile.views import SignInView, KeyStorageCreateView


urlpatterns = patterns('',
    url(r'^signin/$', SignInView.as_view(), name='sign_in'),
    url(r'^key/storage/create/$', KeyStorageCreateView.as_view(), name='key_storage_create'),
)
