# -*- encoding: utf-8 -*-

from apps.profile.models import User, MusicBoxMachine


class CustomUserRequestMiddleware(object):
    """
        Определяем модель юзера, так как стандартная аутинтификация всегда возвразщает модель которая
        указана в настройках
    """
    def process_request(self, request):
        if request.user.is_authenticated():
            try:
                request.user = request.user.musicboxmachine
            except User.DoesNotExist:
                pass
            try:
                request.user = request.user.advertmachine
            except User.DoesNotExist:
                pass

