# -*- encoding: utf-8 -*-
from django import template
from django.db.models.query import ValuesListQuerySet

from apps.gmap.models import GeoData
from pygeocoder import Geocoder, GeocoderError

register = template.Library()


@register.inclusion_tag('gmap.html')
def gmap(*args, **kwargs):
    '''
    Requirements = pip install pygeocoder

    Usage:
    First of all load tag: {% load gmap_tags %}
    And then where you need use google map insert:
    {% gmap 'Russia, Moscow' 'Russia, Krasnoznamensk' %}
    or
    {% gmap 'Russia, Moscow' 'Russia, Krasnoznamensk' size='350x278' zoom=12 %}
    or
    {% gmap 'Russia, Moscow' 'Russia, Krasnoznamensk' zoomControl='false' disableDoubleClickZoom='true' scrollwheel='false' draggable='false' %}
    or
    {% gmap 'Russia, Moscow' 'Russia, Krasnoznamensk' size='350x278' panControl='false' streetViewControl='false' %}
    or
    some_var = Obj.objects.values_list('address')
    {% gmap some_var %}
    '''
    size = kwargs.get('size', '400x400')
    width = '%spx' % size.split('x')[0]
    height = '%spx' % size.split('x')[1]

    if kwargs.get('width'):
        width = kwargs.get('width')
    if kwargs.get('height'):
        height = kwargs.get('height')

    zoom = kwargs.get('zoom', 9)
    zoomControl = kwargs.get('zoomControl', 'true')
    disableDoubleClickZoom = kwargs.get('disableDoubleClickZoom', 'false')
    scrollwheel = kwargs.get('scrollwheel', 'true')
    draggable = kwargs.get('draggable', 'true')
    mapTypeControl = kwargs.get('mapTypeControl', 'true')
    panControl = kwargs.get('panControl', 'true')
    streetViewControl = kwargs.get('streetViewControl', 'true')
    tooltip = kwargs.get('tooltip', 'false')

    coordinates = []
    print args
    for loc in args:
        # if not type(loc) is ValuesListQuerySet:
        #     loc = [loc]

        for x in loc:
            try:
                p = GeoData.objects.get(location=x)
                lat, lng = p.lat, p.lng
            except GeoData.DoesNotExist:
                try:
                    results = Geocoder.geocode(x)
                    results = results[0].coordinates
                    GeoData(location=x, lat=results[0], lng=results[1]).save()
                    lat, lng = results[0], results[1]
                except GeocoderError:
                    lat = lng = ''

            coordinates.append('%s,%s' % (lat, lng))

    return {'width': width, 'height': height,
            'zoom': zoom, 'coordinates': coordinates,
            'zoomControl': zoomControl,
            'disableDoubleClickZoom': disableDoubleClickZoom,
            'scrollwheel': scrollwheel,
            'draggable': draggable,
            'mapTypeControl': mapTypeControl,
            'panControl': panControl,
            'streetViewControl': streetViewControl,
            'tooltip': tooltip,
            'tooltip_data': loc}


@register.filter
def gmap_loc(value, index):
    return value[index][0]
