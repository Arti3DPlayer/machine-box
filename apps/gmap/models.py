# -*- encoding: utf-8 -*-
from django.db import models


class GeoData(models.Model):
    location = models.CharField(max_length=250)
    lat = models.FloatField()
    lng = models.FloatField()

    def __unicode__(self):
        return self.location
