# Music_Box Django Project #
## Prerequisites ##

- python >= 2.5
- pip
- virtualenv/wrapper (optional)

## Installation ##
### Creating the environment ###
Create a virtual python environment for the project.
If you're not using virtualenv or virtualenvwrapper you may skip this step.

#### For virtualenvwrapper ####
```bash
mkvirtualenv --no-site-packages music_box-env
```

#### For virtualenv ####
```bash
virtualenv --no-site-packages music_box-env
cd music_box-env
source bin/activate
```

### Clone the code ###
Obtain the url to your git repository.

```bash
git clone <URL_TO_GIT_RESPOSITORY> music_box
```

### Install requirements ###
```bash
cd music_box
pip install -r requirements.txt
```

### Configure project ###
```bash
cp music_box/__local_settings.py music_box/local_settings.py
vi music_box/local_settings.py
```

### Sync database ###
```bash
python manage.py syncdb
```

## Running ##
```bash
python manage.py runserver
```

Open browser to http://127.0.0.1:8000
