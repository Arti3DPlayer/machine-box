function getCookie(c_name) {
    if (document.cookie.length > 0) {
        var c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            var c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start,c_end));
        }
    }
    return "";
}

function load_music_list(url){
    history.pushState(null, null, url);

    $("#main-navbar .nav li a").each(function(){
        if($(this).attr('href')==url){
            $(this).parent().addClass('active');
        } else {
            $(this).parent().removeClass('active');
        }
    });

    $.ajax({
        url: url,
        method: "GET",
        beforeSend: function() {
            $("#music-list").append('<div class="ajax-overlay">' +
                '<div class="ajax-loader"></div>' +
                '</div>');
        },
        success: function(data){
            $("#music-list").html(data);
        }
    });
    return false;
}

window.addEventListener("popstate", function(e) {
    load_music_list(window.location.pathname+window.location.search);
});