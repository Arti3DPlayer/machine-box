function Playlist(options) {
    this.machine = options.machine;
    this.playlist = [];
    this.audio = new Audio();
    this.current_track = {};

    this.track_loading_flag = false;

    this.random_track_interval = setInterval(this.play_random_track.bind(this), this.machine.settings.delay_before_playing_random_music);
    this.random_track_counter = 0;

    var that = this;
    var track_time_container = setInterval(function(){
        if(that.audio.duration&&that.audio.currentTime) {
            $("#current-song-time").html(that.readable_duration(that.audio.duration - that.audio.currentTime));
        } else {
            $("#current-song-time").html('00:00');
        }
    }, 1000);

    var time_before_play_interval = setInterval(function () {
        var time_before_play_track = 0;
        for (var i = 0; i < that.playlist.length - 1; i++) {
            time_before_play_track += that.playlist[i].duration;
        }
        time_before_play_track += (that.audio.duration - that.audio.currentTime);
        $(".time-before-play-track").html(that.readable_duration(time_before_play_track));
    }, 1000);

    window.addEventListener("click", function(e) {
        clearInterval(that.random_track_interval);
        that.random_track_interval = setInterval(that.play_random_track.bind(that), that.machine.settings.delay_before_playing_random_music);
    });

    $(that.audio).bind('ended error',function(){
        if (that.playlist.length > 0 ) {
            that.current_track = that.playlist.shift();
            that.show_current_track(that.current_track);
            $("#playlist-track-"+that.current_track.id).remove();
            that.audio.src = that.current_track.url;
            that.audio.play();
            that.playlist_is_empty();
            clearInterval(that.random_track_interval);
            that.random_track_interval = setInterval(that.play_random_track.bind(that),
                    (that.current_track.duration*1000)+that.machine.settings.delay_before_playing_random_music);
        } else {
            that.current_track = {};
            $("#current-song").hide();
            clearInterval(that.random_track_interval);
            that.random_track_interval = setInterval(that.play_random_track.bind(that), that.machine.settings.delay_before_playing_random_music);
        }
    });

}

Playlist.prototype.play_random_track = function(){
    var playlist = this;
    clearInterval(playlist.random_track_interval);
    if(playlist.random_track_counter==playlist.machine.settings.delay_before_advert_sound_file) {
        playlist.random_track_counter=0;
        if($.isEmptyObject(playlist.machine.settings.advert_sound_file)) {
            console.log('Начинаем загружать рекламный аудио файл.');
            playlist.add_to_playlist({
                'track': playlist.settings.advert_sound_file
            });
        } else {
            console.log('Рекламный аудиофайл не найден.');
        }
    } else {
        if (playlist.track_loading_flag == false) {
            console.log('Начинаем загружать случайный трек.');
            playlist.track_loading_flag = true;
            $.ajax({
                url: '/music-box/random/track/',
                method: "GET",
                error: function(error) {
                    console.log('Произошла ошибка при загрузке трека. Ищем другой.');
                    playlist.track_loading_flag = false;
                    playlist.play_random_track();
                },
                success: function(data){
                    console.log('Случайный трек выбран:' + data.track.title);
                    playlist.track_loading_flag = false;
                    if($.isEmptyObject(playlist.current_track)) {
                        playlist.add_to_playlist(data);
                        playlist.random_track_counter += 1;
                    } else {
                        console.log('Не возможно играть случайный трек, так как играет пользовательская песня.');
                    }
                }
            });
        } else {
            playlist.random_track_interval = setInterval(playlist.play_random_track.bind(this), playlist.machine.settings.delay_before_playing_random_music);
            console.log('Не возможно начать загрузку случайного трека, другой загружаеться.');
        }
    }
};

Playlist.prototype.buy_track = function(elem, track) {
    var playlist = this;
    console.log('Поиск купленного трека.');
    if ($(elem).find('.ajax-overlay').length) return false;
    $.ajax({
        headers: { "X-CSRFToken": getCookie("csrftoken") },
        url: '/music-box/buy/track/',
        type: "POST",
        data: {
            'track': JSON.stringify(track)
        },
        beforeSend: function() {
            $(elem).append('<div class="ajax-overlay">' +
                '<div class="ajax-loader"></div>' +
                '</div>');
        },
        error: function(error) {
            console.log('Произошла ошибка во время покупки трека.');
            $(elem).find('.ajax-overlay').remove();
            playlist.machine.show_message_dialog({
                message: 'Произошла ошибка во время покупки трека.'
            });
        },
        success: function (data) {
            $(elem).find('.ajax-overlay').remove();
            if(data.status == "success"){
                console.log('Купленный трек найден.');
                $(".cash").html(data.cash);
                playlist.add_to_playlist({
                    'track': data.track,
                    'message': data.message
                });
            } else {
                console.log('Произошла ошибка во время покупки трека.');
                playlist.machine.show_message_dialog({
                    message: data.message
                });
            }
        }
    });

    return false;
};

Playlist.prototype.add_to_playlist = function (data) {
    var playlist = this;
    var track = data.track;
    var playlist_track_id = playlist.playlist.push(track);
    playlist.playlist[playlist_track_id-1]['id'] = playlist_track_id;
    clearInterval(playlist.random_track_interval);

    if (playlist.playlist.length > 0 && !playlist.audio.paused ) {
        data.message += '<p><b>Заказанный трек будет воспроизведён через</b>:' +
            ' <span class="time-before-play-track"></span></p>';
    }

    if (!playlist.audio.paused) {
        console.log("Трек "+playlist.current_track.title+" уже играет. Добавляем текущий в плейлист.");
        playlist.playlist_is_empty();
        $("#playlist").append('<div class="track-line-container" id="playlist-track-'+playlist_track_id+'">'+
            '<marquee class="track-content" scrollamount="1" behavior="alternate" direction="right">'+
            '<strong>'+track.artist+'</strong> - '+track.title+''+
            '</marquee>'+
            '</div>');
        playlist.machine.show_message_dialog({
            message: data.message,
            timeout: playlist.machine.settings.delay_before_playing_music
        });
        console.log("Готово.");
    } else {
        playlist.current_track = playlist.playlist.shift();
        playlist.show_current_track(playlist.current_track);

        if (!playlist.current_track.random_track) {
            console.log("Начинаем воспроизводить выбранный трек.");
            playlist.audio.src = track.url;
            playlist.audio.load();
            playlist.machine.show_message_dialog({
                message: data.message,
                timeout: playlist.machine.settings.delay_before_playing_music
            });
            setTimeout(function() {
                playlist.audio.play();
                console.log("Готово.");
            }, playlist.machine.settings.delay_before_playing_music);

        } else {
            console.log("Начинаем воспроизводить случайный трек.");
            playlist.audio.src = track.url;
            playlist.audio.load();
            playlist.audio.play();
            console.log("Готово.");
        }
    }

};

Playlist.prototype.show_current_track = function (track) {
    var playlist = this;
    $("#current-song").show();
    $("#current-song-title").html(track.title);
    $("#current-song-time").html(playlist.readable_duration(track.duration/1000));
    $("#current-song-artist").html(track.artist);
    $("#current-song-album").html(track.album_name);
    $("#current-song-image").attr('src', track.album_preview);
};

Playlist.prototype.playlist_is_empty = function(){
    var playlist =this;
    if (playlist.playlist.length > 0 && !$("#playlist .track-line-container").length) {
        $("#playlist").html('');
    } else if(playlist.playlist.length == 0) {
        $("#playlist").html('<p class="empty-playlist">' +
            'Плейлист пока пуст.' +
            '</p>');
    }
};

Playlist.prototype.readable_duration = function (seconds) {
    var sec = Math.floor(seconds);
    var min = Math.floor(sec/60);
    min = min >= 10 ? min : '0' + min;
    sec = Math.floor( sec % 60 );
    sec = sec >= 10 ? sec : '0' + sec;
    return min + ':' + sec;
};