function connected_socket(params) {
    if (params.socket&&params.channel){
        params.socket.subscribe(params.channel);
    }
    if (params.callback) params.callback();
}

function disconnected_socket(params) {
    if (params.callback) {
        params.callback();
    }
}

function getCookie(c_name) {
    if (document.cookie.length > 0) {
        var c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1) {
            c_start = c_start + c_name.length + 1;
            var c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start,c_end));
        }
    }
    return "";
}


function get_machine_settings() {
    return JSON.parse($.ajax({
        url: '/get/machine/settings/',
        type: "GET",
        async: false
    }).responseText);
}

function convert_credit_cash(){
    $.ajax({
        url: '/convert/credit_to_balance/',
        type: "GET"
    });
    return false;
}

function return_money(){
    $.ajax({
        url: '/return/money/',
        type: "GET"
    });
    return false;
}

function show_advert_modal(){
    if (!$("#alert-modal").hasClass('in')) {
        $.ajax({
            url: "/get/machine/advertisment/slider/",
            method: "GET",
            error: function(data){
                console.log(data)
            },
            success: function(data){
                show_modal({
                    message: data,
                    size: 'modal-lg'
                });
                clearInterval(ADVERT_INTERVAL);
            }
        });
    }
}

function show_modal(data){
    var message = data.message||'';
    var timeout = data.timeout;
    var close_onclick = true;
    var size = data.size||'modal-md';
    var important = data.important;
    var callback = data.callback;
    if (data.close_onclick===false){ close_onclick = false}
    $("#alert-modal .modal-body").html(message);
    $('#alert-modal').data('close_onclick', close_onclick);
    $('#alert-modal .modal-dialog').removeClass('modal-lg modal-md').addClass(size);
    if (!$("#alert-modal").hasClass('in')) {
        $('#alert-modal').modal('show');
        if (timeout) {
            setTimeout(function() {
                $('#alert-modal').modal('hide');
            }, timeout);
        }
        $('#alert-modal').on('hidden.bs.modal', function (e) {
            if(callback) {
                callback();
            }
            $('#alert-modal').unbind('hidden.bs.modal');
        });
    }
}


function play_random_track() {
    clearInterval(RANDOM_TRACK_INTERVAL);
    if(RANDOM_TRACK_COUNTER-1==MACHINE_SETTINGS.delay_before_advert_sound_file && !$.isEmptyObject(MACHINE_SETTINGS.advert_sound_file)) {
        RANDOM_TRACK_COUNTER=0;
        add_to_playlist({'track': MACHINE_SETTINGS.advert_sound_file});
    } else {
        $.ajax({
            url: "/random/track/",
            method: "GET",
            success: function(data){
                add_to_playlist(data);
            }
        });
    }
    RANDOM_TRACK_COUNTER++;
}

window.addEventListener("popstate", function(e) {
    load_music_list(window.location.pathname+window.location.search);
});

window.addEventListener("click", function(e) {
    clearInterval(ADVERT_INTERVAL);
    if($('#alert-modal').data('close_onclick')) {
        $('#alert-modal').modal('hide');
    }
    ADVERT_INTERVAL = setInterval(show_advert_modal, MACHINE_SETTINGS.delay_before_starting_advertising);
});

function load_music_list(url){
    if (url == "/") url='/music/genre/list/';
    history.pushState(null, null, url);

    $("#music_box-navbar .nav li a").each(function(){
        if($(this).attr('href')==url){
            $(this).parent().addClass('active');
        } else {
            $(this).parent().removeClass('active');
        }
    });

    $.ajax({
        url: url,
        method: "GET",
        beforeSend: function() {
            $("#music-list").append('<div class="ajax-overlay">' +
                                    '<div class="ajax-loader"></div>' +
                                    '</div>');
        },
        success: function(data){
            $("#music-list").html(data);
        }
    });
    return false;
}


function buy_track(elem, track) {
    if ($(elem).find('.ajax-overlay').length) return false;
    $.ajax({
        headers: { "X-CSRFToken": getCookie("csrftoken") },
        url: '/buy/track/',
        type: "POST",
        data: {
            'track': JSON.stringify(track)
        },
        beforeSend: function() {
            $(elem).append('<div class="ajax-overlay">' +
                                    '<div class="ajax-loader"></div>' +
                                    '</div>');
        },
        success: function (data) {
            $(elem).find('.ajax-overlay').remove();
            if(data.status == "success"){
                $(".cash").html(data.cash);
                add_to_playlist({
                    'track': data.track,
                    'success_message': data.message
                });
            } else {
                show_modal({'message': data.message});
            }
        }
    });

    return false;
}

function readable_duration(seconds) {
    var sec = Math.floor(seconds);
    var min = Math.floor(sec/60);
    min = min >= 10 ? min : '0' + min;
    sec = Math.floor( sec % 60 );
    sec = sec >= 10 ? sec : '0' + sec;
    return min + ':' + sec;
}


function add_to_playlist(data) {
    var track = data.track;
    var success_message = data.success_message;
    var playlist_track_id = PLAYLIST.push(track);
    PLAYLIST[playlist_track_id-1]['id'] = playlist_track_id;
    if(!track.random_track) {
        if (PLAYLIST.length > 0 && !AUDIO.paused ) {
            success_message += '<p><b>Заказанный трек будет воспроизведён через</b>:' +
                ' <span class="time-before-play-track"></span></p>';
            var time_before_play_interval = setInterval(function () {
                var time_before_play_track = 0;
                for (var i = 0; i < PLAYLIST.length - 1; i++) {
                    time_before_play_track += PLAYLIST[i].duration;
                }
                time_before_play_track += AUDIO.duration - AUDIO.currentTime;
                $(".time-before-play-track").html(readable_duration(time_before_play_track));
            }, 1000);
        }
        show_modal({
            'message': success_message,
            'timeout': MACHINE_SETTINGS.delay_before_playing_music,
            'callback': function () {
                clearInterval(time_before_play_interval);
            }
        });
    }
    if (!AUDIO.paused) {
        playlist_is_empty();
        $("#playlist").append('<div class="track-line-container" id="playlist-track-'+playlist_track_id+'">'+
            '<marquee class="track-content" scrollamount="1" behavior="alternate" direction="right">'+
            '<strong>'+track.artist+'</strong> - '+track.title+''+
            '</marquee>'+
            '</div>');
    } else {
        CURRENT_TRACK = PLAYLIST.shift();
        $("#current-song").show();
        $("#current-song-title").html(track.title);
        $("#current-song-time").html(readable_duration(track.duration/1000));
        $("#current-song-artist").html(track.artist);
        $("#current-song-album").html(track.album_name);
        $("#current-song-image").attr('src', track.album_preview);

        if (!CURRENT_TRACK.random_track) {
            AUDIO.src = track.url;
            AUDIO.load();
            setTimeout(function() {
                AUDIO.play();
                TRACK_TIME_INTERVAL = setInterval(function(){
                    $("#current-song-time").html(readable_duration(AUDIO.duration - AUDIO.currentTime));
                }, 1000);
            }, MACHINE_SETTINGS.delay_before_playing_music);


            clearInterval(RANDOM_TRACK_INTERVAL);
            RANDOM_TRACK_INTERVAL = setInterval(play_random_track,
                (CURRENT_TRACK.duration*1000)+MACHINE_SETTINGS.delay_before_playing_random_music);
        } else {
            AUDIO.src = track.url;
            AUDIO.load();
            AUDIO.play();
            TRACK_TIME_INTERVAL = setInterval(function(){
                $("#current-song-time").html(readable_duration(AUDIO.duration - AUDIO.currentTime));
            }, 1000);

            clearInterval(RANDOM_TRACK_INTERVAL);
            RANDOM_TRACK_INTERVAL = setInterval(play_random_track,
                (CURRENT_TRACK.duration*1000));
        }



    }

    return false;
}

function playlist_is_empty() {
    if (PLAYLIST.length > 0 && !$("#playlist .track-line-container").length) {
        $("#playlist").html('');
    } else if(PLAYLIST.length == 0) {
        $("#playlist").html('<p class="empty-playlist">' +
            'Плейлист пока пуст.' +
            '</p>');
    }
}
