function Machine(options) {
    this.id = options.id;
    this.settings = JSON.parse($.ajax({
        url: '/music-box/get/settings/',
        type: "GET",
        async: false
    }).responseText);

    this.advert_timeout = setTimeout(this.show_advert_dialog.bind(this), this.settings.delay_before_starting_advertising);
    var that = this;
    window.addEventListener("click", function(e) {
        clearTimeout(that.advert_timeout);
        that.advert_timeout = setTimeout(that.show_advert_dialog.bind(that), that.settings.delay_before_starting_advertising);
    });

    var messaged_socket = function(data) {
        switch (data.action) {
            case 'update_cash':
                $(".cash").html(data.cash);
                if (data.credit_cash > 0) {
                    that.show_message_dialog({
                        message: data.message,
                        not_close_onclick: true,
                        timeout: 60000
                        //callback: that.convert_credit_to_cash
                    });
                }
        }
    };

    var start_socket = function() {
        var socket = new io.Socket();
        socket.connect();
        socket.on('connect', function(){
            socket.subscribe('machine-'+that.id);
        });
        socket.on('disconnect', function(){
            setTimeout(start_socket, 1000);
        });
        socket.on('message', messaged_socket);
    }();

}

Machine.prototype.show_advert_dialog = function () {
    var machine = this;
    if(machine.dialog) {
        console.log('Другое диалоговое окно уже показываеться.');
        clearTimeout(machine.advert_timeout);
        machine.advert_timeout = setTimeout(machine.show_advert_dialog.bind(machine), machine.settings.delay_before_starting_advertising);
    } else {
        var container = $('<div/>', {
            id: 'advert-dialog'
        }).appendTo('body');
        machine.dialog = $(container).dialog({
            autoOpen: false,
            resizable: false,
            draggable: false,
            closeOnEscape: true,
            modal: true,
            width: '90%',
            minHeight: 'auto',
            create: function(event, ui){
                var dialog = $(this);
                $(".ui-widget-header").hide();
                console.log('Загружаем рекламный слайдер.');
                $.ajax({
                    url: "/music-box/advertisment/slider/",
                    method: "GET",
                    success: function(data){
                        $(dialog).html(data);
                        $(dialog).dialog('open');
                        console.log('Готово.');
                    }
                });
            },
            open: function(event, ui){
                var dialog = $(this);
                $('body').css('overflow','hidden');/* Scrollbar fix */
                $('.ui-widget-overlay').bind('click',function(){
                    $(dialog).dialog('close');
                });
            },
            close: function(event, ui){
                $('body').css('overflow','auto');/* Scrollbar fix */
                $(this).dialog('destroy').remove();
                machine.advert_timeout = setTimeout(machine.show_advert_dialog.bind(machine), machine.settings.delay_before_starting_advertising);
                machine.dialog = null;
            }
        });
    }

};

Machine.prototype.show_message_dialog = function (options) {
    var machine = this;
    var timeout_dialog;
    clearTimeout(machine.advert_timeout);
    if(machine.dialog) {
        machine.dialog.dialog('close');
    }
    var container = $('<div/>', {
        id: 'message-dialog'
    }).appendTo('body');
    machine.dialog = $(container).dialog({
        resizable: false,
        draggable: false,
        closeOnEscape: true,
        modal: true,
        minHeight: 'auto',
        width: "auto",
        create: function(event, ui){
            var dialog = $(this);
            $(".ui-widget-header").hide();
            $(dialog).html(options.message);
            if(options.timeout) {
                timeout_dialog = setTimeout(function(){
                    $(dialog).dialog('close');
                }, options.timeout);
            }
        },
        open: function(event, ui){
            var dialog = $(this);
            $('body').css('overflow','hidden');/* Scrollbar fix */
            if(!options.not_close_onclick){
                $('.ui-widget-overlay').bind('click',function(){
                    $(dialog).dialog('close');
                });
            }
        },
        close: function(event, ui){
            $('body').css('overflow','auto');/* Scrollbar fix */
            clearTimeout(machine.advert_timeout);
            machine.advert_timeout = setTimeout(machine.show_advert_dialog.bind(machine), machine.settings.delay_before_starting_advertising);
            $(this).dialog('destroy').remove();
            machine.dialog = null;
            clearTimeout(timeout_dialog);
            if(options.callback){
                options.callback();
            }
        }
    });

};

Machine.prototype.convert_credit_to_cash = function (){
    var machine = this;
    $.ajax({
        url: '/music-box/convert/credit_to_balance/',
        method: "GET",
        success: function(data){
            $(".cash").html(data.cash);
            if(machine.dialog) {
                machine.dialog.dialog('close');
            }
        }
    });
    return false;
};


Machine.prototype.give_change = function (){
    var machine = this;
    console.log('Начинаем выдавать сдачу.');
    $.ajax({
        url: '/music-box/give/change/confirm/',
        method: "GET",
        success: function(data){
            if (data.cash) {
                $(".cash").html(data.cash);
            }
            if(machine.dialog) {
                machine.dialog.dialog('close');
            }
            machine.show_message_dialog({
                timeout: 10000,
                message: data.message
            });
        }
    });
    return false;
};