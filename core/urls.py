from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    url(r'', include('main.urls', 'main')),
    url(r'^music-box/', include('music_box.urls', 'music_box')),
    url(r'^advert-box/', include('advert_box.urls', 'advert_box')),
    url(r'', include('profile.urls', 'profile')),

    url(r'', include('django_socketio.urls')),
    url(r'^09061984/filebrowser/', include('filebrowser.urls')),
    url(r'^09061984/', include(admin.site.urls)),
)


if settings.DEBUG:
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT,}),
    )
